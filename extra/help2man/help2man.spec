Name:		help2man
Version:	1.48.3
Release:	1
Summary:	Create simple man pages from --help output
License:	GPLv3+
URL:		https://www.gnu.org/software/help2man/

Source0:	https://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz

BuildArch:	noarch

Requires:	perl

%description
help2man is a script to create simple man pages from the --help and
--version output of programs.

Since most GNU documentation is now in info format, this provides a
way to generate a placeholder man page pointing to that resource while
still providing some useful information.

%prep
%setup -q

%build
export ac_cv_path_MSGFMT=true

%configure \
	--disable-nls
%make_build

%install
%make_install

%files
%doc README NEWS THANKS
%license COPYING
%defattr(-,root,root)
%{_bindir}/help2man
%{_infodir}/*
%{_mandir}/*

%changelog
* Wed Jun 30 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 1.48.3-1
- Initial package
