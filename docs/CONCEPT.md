# Concept
Ataraxia GNU/Linux follows certain concepts, they define design of the distribution and how should things be done:

## Development
Here's development concepts to follow:
```
 * No automatic package updates
 * Writing simple and understandable code
 * Do not repeat yourself
 * Test before pushing changes
 * Do work in separate branches
```

## Distribution
Here's concepts for distibution design:
```
 * Using musl libc as default libc
 * Using pure LLVM toolchain
 * Using toybox as coreutils implemenation
 * Using less GNU and GPL packages in base system (development tools are the exception)
 * Using special compiler options to increase security (-D_FORTIFY_SOURCE=2, PIC, PIE, Strong Stack Protection, Stack Clashing Protection, RELRO, Bind Now, retpoline, CFI, Shadow Call Stack)
 * Using hardened Linux kernel
 * Using RPM package manager
 * Using OpenSSL as SSL/TLS implemenation
 * Using encrypted storaging by default, not letting user to choose
 * Being stateless OS by default
 * Follow FSF rules for package inclusion, non-free packages are prohibited (Blacklist of packages: https://git.parabola.nu/blacklist.git/plain/blacklist.txt and https://git.parabola.nu/blacklist.git/plain/your-privacy-blacklist.txt exceptions are: linux, linux-firmware, rust, spidermonkey and flatpak)
 * If it's possible to remove dependency on non-free package or remove tracking stuff then package can be included
 * No deprecated/legacy hardware support, libraries and programs
 * Wayland and GTK+ centric
 * Main desktops will be GNOME, MATE, Xfce, Weston, Sway, Enlightenment, i3{,-gaps}, dwm, ratpoison, icewm, herbstluftwm, velox, openbox
 * Other programs and desktop must be maintained outside of main repository
```
