%global _visibility_flags -fvisibility=default

Name:		netbsd-curses
Version:	0.3.2
Release:	2
Summary:	A port of NetBSD curses library on Linux
License:	BSD
URL:		https://github.com/sabotage-linux/netbsd-curses/

Source0:	http://ftp.barfooze.de/pub/sabotage/tarballs/%{name}-%{version}.tar.xz
Source1:	ncurses5-config

Patch0:		netbsd-curses-enable-compat.patch
Patch1:		netbsd-curses-libtinfo-symlink.patch

%description
This is port of NetBSD curses library to use on Linux systems.

%prep
%setup -q
%patch0 -p1
%patch1 -p1

%build
%set_build_flags

cat << EOF > config.mak
CC=%{__cc}
HOSTCC=%{__cc_host}
AR=%{__ar}
RANLIB=%{__ranlib}
CFLAGS=-fPIC %{optflags}
LDFLAGS=%{build_ldflags}
PREFIX=%{_prefix}
EOF

%make_build

%install
%make_install

rm \
	"%{buildroot}"%{_mandir}/man3/attr_get.3 \
	"%{buildroot}"%{_mandir}/man3/attr_set.3

install -Dm755 "%{SOURCE1}" "%{buildroot}"%{_bindir}/ncurses5-config
ln -sf ncurses5-config "%{buildroot}"%{_bindir}/ncursesw5-config

%if "%{buildtype}" == "cross"
install -Dm755 "%{SOURCE1}" "%{tools}"/bin/%{_xtarget}-ncurses5-config
ln -sf %{_xtarget}-ncurses5-config "%{tools}"/bin/%{_xtarget}-ncursesw5-config
%endif

%files
%doc README.md
%license COPYING
%defattr(-,root,root)
%{_bindir}/*
%{_libdir}/*
%{_includedir}/*
%{_mandir}/*

%changelog
* Mon Jun 21 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 0.3.2-2
- Fix conflicting manual pages
* Sat Jun 19 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 0.3.2-1
- Initial package
