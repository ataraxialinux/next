Name:		attr
Version:	2.5.1
Release:	1
Summary:	Utilities for managing filesystem extended attributes
License:	GPLv2+
URL:		https://savannah.nongnu.org/projects/attr/

Source0:	https://download-mirror.savannah.gnu.org/releases/%{name}/%{name}-%{version}.tar.gz
Source1:	attr.tmpfiles

%description
A set of tools for manipulating extended attributes on filesystem
objects, in particular getfattr(1) and setfattr(1).
An attr(1) command is also provided which is largely compatible
with the SGI IRIX tool of the same name.

%prep
%setup -q

%build
%configure \
	--disable-nls
%make_build

%install
%make_install

install -Dm644 "%{SOURCE1}" "%{buildroot}"%{_libdir}/tmpfiles.d/%{name}.conf

install -dm755 "%{buildroot}"%{_factorysysconfdir}
mv "%{buildroot}"%{_sysconfdir}/xattr.conf "%{buildroot}"%{_factorysysconfdir}/

%files
%doc doc/CHANGES
%license doc/COPYING*
%defattr(-,root,root)
%{_bindir}/*
%{_docdir}/%{name}/COPYING*
%{_docdir}/%{name}/PORTING
%{_factorysysconfdir}/*
%{_includedir}/*
%{_libdir}/*
%{_mandir}/*

%changelog
* Mon Jun 21 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 2.5.1-1
- Initial package
