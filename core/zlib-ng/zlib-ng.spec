%global _visibility_flags -fvisibility=default

Name:		zlib-ng
Version:	2.0.4
Release:	1
Summary:	zlib data compression and decompression library for the next generation systems
License:	zlib
URL:		https://github.com/zlib-ng/zlib-ng/

Source0:	https://github.com/zlib-ng/zlib-ng/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires:	cmake
BuildRequires:	ninja

%description
zlib data compression and decompression library for the next generation systems.

%prep
%setup -q

%build
%cmake \
	-DZLIB_COMPAT=ON
%cmake_build

%install
%cmake_install

%files
%doc README.md
%license LICENSE.md
%defattr(-,root,root)
%{_includedir}/*
%{_libdir}/*

%changelog
* Sat Jun 19 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 1.0.8-1
- Initial package
