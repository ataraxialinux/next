%define _lto_cflags %{nil}
%define _cfi_cflags %{nil}
%define _safestack_cflags %{nil}
%define _scs_cflags %{nil}
%define _scudo_cflags %{nil}

Name:		libmexecinfo
Version:	0.2
Release:	1
Summary:	libexecinfo extracted from FreeBSD and NetBSD for musl systems
License:	BSD
URL:		https://gitlab.com/ataraxialinux/libmexecinfo/

Source0:	https://gitlab.com/ataraxialinux/libmexecinfo/-/archive/%{version}/%{name}-%{version}.tar.gz

Patch0:		libmexecinfo-0001-fixes-for-makefile.patch

%description
libexecinfo is a library which was extracted from FreeBSD.
This library is used to provide `backtrace(3)` function for musl systems.
It can help with porting glibc-specific code on other system and
debug your program.

%prep
%setup -q
%patch0 -p1

%build
%set_build_flags

%make_build

%install
mkdir -p "%{buildroot}%{_mandir}/man3"

%make_install PREFIX="%{_prefix}"

%files
%defattr(-,root,root)
%{_libdir}/*
%{_includedir}/*
%{_mandir}/*

%changelog
* Mon Jun 14 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 0.2-1
- Initial package
