%global _visibility_flags -fvisibility=default

Name:		gdbm
Version:	1.20
Release:	1
Summary:	A GNU set of database routines which use extensible hashing
License:	GPLv3+
URL:		https://www.gnu.org/software/gdbm/

Source0:	https://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.gz

BuildRequires:	libedit

%description
gdbm is a GNU database indexing library, including routines which use
extensible hashing.  Gdbm works in a similar way to standard UNIX dbm
routines.  Gdbm is useful for developers who write C applications and
need access to a simple and efficient database or who are building C
applications which will use such a database.

%prep
%setup -q

%build
%configure \
	--enable-libgdbm-compat \
	--disable-nls

sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

%make_build

%install
%make_install

%files
%doc NEWS README THANKS AUTHORS NOTE-WARNING
%license COPYING
%defattr(-,root,root)
%{_bindir}/*
%{_includedir}/*
%{_infodir}/*
%{_libdir}/*
%{_mandir}/*

%changelog
* Thu Jul 01 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 1.20-1
- Initial package
