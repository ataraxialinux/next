%global _vpath_srcdir contrib/meson
%global _visibility_flags -fvisibility=default

Name:		lz4
Version:	1.9.3
Release:	1
Summary:	Extremely fast compression algorithm
License:	GPLv2+ and BSD
URL:		https://lz4.github.io/lz4/

Source0:	https://github.com/lz4/lz4/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:	meson

%description
LZ4 is an extremely fast loss-less compression algorithm, providing compression
speed at 400 MB/s per core, scalable with multi-core CPU. It also features
an extremely fast decoder, with speed in multiple GB/s per core, typically
reaching RAM speed limits on multi-core systems.

%prep
%setup -q

%build
%meson \
	-Dbin_programs=true \
	-Ddefault_library=both \
	%{nil}
%meson_build

%install
%meson_install

%files
%doc NEWS
%license programs/COPYING lib/LICENSE
%defattr(-,root,root)
%{_bindir}/*
%{_libdir}/*
%{_includedir}/*
%{_mandir}/*

%changelog
* Fri Jun 18 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 1.9.3-1
- Initial package
