%define _lto_cflags %{nil}
%define _cfi_cflags %{nil}
%define _safestack_cflags %{nil}
%define _scs_cflags %{nil}
%define _scudo_cflags %{nil}

Name:		musl
Version:	1.2.2
Release:	2
Summary:	Fully featured lightweight standard C library for Linux
License:	MIT
URL:		https://musl.libc.org/

Source0:	https://musl.libc.org/releases/%{name}-%{version}.tar.gz

Provides:	rtld(GNU_HASH)

%description
musl is a C standard library to power a new generation
of Linux-based devices. It is lightweight, fast, simple,
free, and strives to be correct in the sense of standards
conformance and safety.

%prep
%setup -q

%build
%configure \
	--syslibdir=%{_libdir} \
	--enable-optimize=size
%make_build

%install
%make_install

%files
%defattr(-,root,root)
%{_libdir}/*
%{_includedir}/*

%changelog
* Thu Jul 01 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 1.2.2-2
- Provide rtld(GNU_HASH)
* Mon Jun 14 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 1.2.2-1
- Initial package
