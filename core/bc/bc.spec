Name:		bc
Version:	4.0.2
Release:	1
Summary:	An implementation of Unix dc and POSIX bc with GNU and BSD extensions
License:	custom
URL:		https://github.com/gavinhoward/bc

Source0:	https://github.com/gavinhoward/bc/releases/download/%{version}/%{name}-%{version}.tar.xz

%description
This is an implementation of the POSIX bc calculator that implements
GNU bc extensions, as well as the period extension for the BSD flavor of bc.

%prep
%setup -q

%build
%set_build_flags
PREFIX=%{_prefix} \
./configure
%make_build PREFIX=%{_prefix}

%install
%make_install PREFIX=%{_prefix}

rm -rf \
	"%{buildroot}"%{_datadir}/locale

%files
%doc README.md NEWS.md NOTICE.md
%license LICENSE.md
%defattr(-,root,root)
%{_bindir}/*
%{_mandir}/*

%changelog
* Mon Jun 21 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 4.0.2-1
- Initial package
