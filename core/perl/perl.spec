%define crossver 1.3.6

%define _lto_cflags %{nil}
%define _cfi_cflags %{nil}

Name:		perl
Version:	5.34.0
Release:	1
Summary:	Practical Extraction and Report Language
License:	GPL+ and Artistic
URL:		https://www.perl.org/

Source0:	https://www.cpan.org/src/5.0/perl-%{version}.tar.xz
Source1:	https://github.com/arsv/perl-cross/releases/download/%{crossver}/perl-cross-%{crossver}.tar.gz

Patch0:		perl-musl-locale.patch
Patch1:		perl-musl-stack-size.patch

BuildRequires:	zlib-ng
BuildRequires:	bzip2
BuildRequires:	berkeley-db
BuildRequires:	gdbm

Provides:	/usr/bin/perl

%description
Perl is a high-level programming language with roots in C, sed, awk and shell
scripting. Perl is good at handling processes and files, and is especially
good at handling text. Perl's hallmarks are practicality and efficiency.
While it is used to do a lot of different things, Perl's most common
applications are system administration utilities and web programming.

This is a metapackage with all the Perl bits and core modules that can be
found in the upstream tarball from perl.org.

%prep
%setup -q -b 1 -n perl-cross-%{crossver}
%setup -q -b 0
%patch0 -p1
%patch1 -p1

cp -fvr ../perl-cross-%{crossver}/* .

%build
%set_build_flags

export BUILD_ZLIB=False
export BUILD_BZIP2=0

./configure %{__perlflags} \
	--prefix=%{_prefix} \
	-Dvendorprefix=%{_prefix} \
	-Dsiteprefix=%{_prefix}/local \
	-Dprivlib=%{_libdir}/perl5/core_perl \
	-Darchlib=%{_libdir}/perl5/core_perl \
	-Dsitelib=%{_prefix}/local/share/perl5/site_perl \
	-Dsitearch=%{_prefix}/local/lib/perl5/site_perl \
	-Dvendorlib=%{_datadir}/perl5/vendor_perl \
	-Dvendorarch=%{_libdir}/perl5/vendor_perl \
	-Dman1dir=%{_mandir}/man1 \
	-Dman3dir=%{_mandir}/man3 \
	-Dpager="%{_bindir}/less -isR" \
	-Dcf_by='Ataraxia GNU/Linux' \
	-Dmyuname='localhost' \
	-Dmyhostname='localhost' \
	-Accflags='-D_GNU_SOURCE' \
	-Doptimize="%{optflags}" \
	-Duselargefiles \
	-Duseshrplib \
	-Dusethreads
%make_build libperl.so
%make_build

%install
%make_install

rm -rf "%{buildroot}"%{_prefix}/local

%files
%doc AUTHORS README Changes
%license Artistic Copying
%defattr(-,root,root)
%{_bindir}/*
%{_libdir}/*
%{_mandir}/*

%changelog
* Thu Jul 01 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 5.34.0-1
- Initial package
