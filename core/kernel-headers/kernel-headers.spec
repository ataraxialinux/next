Name:		kernel-headers
Version:	5.12.12
Release:	1
Summary:	Header files for the Linux kernel for use by standard C library
License:	GPLv2
URL:		https://www.kernel.org/

Source0:	https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-%{version}.tar.xz

%description
Kernel-headers includes the C header files that specify the interface
between the Linux kernel and userspace libraries and programs.  The
header files define structures and constants that are needed for
building most standard programs.

%prep
unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

%setup -q -n linux-%{version}
%make_build mrproper

%build
unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

case "%{_target_cpu}" in
	x86_64)
		KARCH="x86_64"
		;;
	aarch64)
		KARCH="arm64"
		;;
	ppc64le)
		KARCH="powerpc"
		;;
	ppc64)
		KARCH="powerpc"
		;;
	riscv64)
		KARCH="riscv"
		;;
	*)
		echo 'Architecture called `'%{_target_cpu}'` is not supported by Ataraxia GNU/Linux'
		exit 1
		;;
esac

%make_build ARCH="$KARCH" headers

%install
mkdir -pv "%{buildroot}"%{_includedir}

find usr/include -name '.*' -delete
rm -v usr/include/Makefile
cp -av usr/include/* "%{buildroot}"%{_includedir}

find "%{buildroot}" \( -name .install -o -name ..install.cmd \) -print0 | xargs -0 rm -rvf

%files
%defattr(-,root,root)
%{_includedir}/*

%changelog
* Fri Jun 18 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 5.12.12-1
- Bump kernel-headers to 5.12.12
* Sun Jun 13 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 5.12.10-1
- Initial package
