%define _lto_cflags %{nil}
%define _cfi_cflags %{nil}

%ifarch x86_64
%define openssl_target linux-x86_64
%define openssl_options enable-ec_nistp_64_gcc_128
%endif
%ifarch aarch64
%define openssl_target linux-aarch64
%endif
%ifarch ppc64le
%define openssl_target linux-ppc64le
%endif
%ifarch ppc64
%define openssl_target linux-ppc64
%define openssl_options no-asm
%endif
%ifarch riscv64
%define openssl_target linux-generic64
%endif


Name:		openssl
Version:	1.1.1k
Release:	1
Summary:	Utilities from the general purpose cryptography library with TLS implementation.
License:	OpenSSL and ASL 2.0
URL:		https://www.openssl.org/

Source0:	https://www.openssl.org/source/%{name}-%{version}.tar.gz
Source1:	openssl.tmpfiles

BuildRequires:	perl
BuildRequires:	zlib-ng

%description
The OpenSSL toolkit provides support for secure communications between
machines. OpenSSL includes a certificate management tool and shared
libraries which provide various cryptographic algorithms and
protocols.

%package runtime
Summary:	A general purpose cryptography library with TLS implementation
Recommends:	ca-certificates

%description runtime
OpenSSL is a toolkit for supporting cryptography. The openssl-runtime
package contains the libraries that are used by various applications which
support cryptographic algorithms and protocols.

%prep
%setup -q

%build
%set_build_flags

%if "%buildtype" == "cross"
unset CROSS_COMPILE
%endif

./Configure %{openssl_target} \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--openssldir=%{_sysconfdir}/ssl \
	shared no-ssl3-method %{?openssl_options} \
	%{optflags} %{build_ldflags} -Wa,--noexecstack
%make_build

%install
%make_install

install -Dm644 "%{SOURCE1}" "%{buildroot}"%{_libdir}/tmpfiles.d/openssl.conf

install -dm755 "%{buildroot}"%{_factorysysconfdir}/ssl
mv "%{buildroot}"%{_sysconfdir}/ssl/openssl.cnf "%{buildroot}"%{_factorysysconfdir}/ssl/openssl.cnf
mv "%{buildroot}"%{_sysconfdir}/ssl/ct_log_list.cnf "%{buildroot}"%{_factorysysconfdir}/ssl/ct_log_list.cnf

rm -rf "%{buildroot}"%{_sysconfdir}

%files
%doc FAQ NEWS README CHANGES doc/dir-locals.example.el doc/openssl-c-indent.el
%license LICENSE
%defattr(-,root,root)
%{_bindir}/*
%{_includedir}/*
%{_docdir}/%{name}/*
%{_factorysysconfdir}/*
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_libdir}/tmpfiles.d/*.conf
%{_mandir}/*

%files runtime
%license LICENSE
%defattr(-,root,root)
%{_libdir}/*.so.*
%{_libdir}/engines-*/*.so

%changelog
* Thu Jul 01 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 1.1.1k-1
- Initial package
