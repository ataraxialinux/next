%define srcver 3360000

Name:		sqlite
Version:	3.36.0
Release:	1
Summary:	Library that implements an embeddable SQL database engine
License:	Public Domain
URL:		https://www.sqlite.org/index.html

Source0:	https://www.sqlite.org/2021/sqlite-autoconf-%{srcver}.tar.gz
Source1:	https://sqlite.org/2021/sqlite-doc-%{srcver}.zip

%description
SQLite is a C library that implements an SQL database engine. A large
subset of SQL92 is supported. A complete database is stored in a
single disk file. The API is designed for convenience and ease of use.
Applications that link against SQLite can enjoy the power and
flexibility of an SQL database without the administrative hassles of
supporting a separate database server.  Version 2 and version 3 binaries
are named to permit each to be installed on a single host.

%package documentation
Summary: C development documentation files for the Berkeley DB library
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description documentation
This package contains most of the static HTML files that comprise the
www.sqlite.org website, including all of the SQL Syntax and the
C/C++ interface specs and other miscellaneous documentation.

BuildRequires:	libedit
BuildRequires:	zlib-ng

%prep
%setup -q -b 1 -n sqlite-doc-%{srcver}
%setup -q -b 0 -n sqlite-autoconf-%{srcver}

%build
export CPPFLAGS="-DSQLITE_ENABLE_COLUMN_METADATA=1 \
	-DSQLITE_DISABLE_DIRSYNC=1 -DSQLITE_ENABLE_FTS3=1 \
	-DSQLITE_ENABLE_RTREE=1 -DSQLITE_SECURE_DELETE=1 \
	-DSQLITE_ENABLE_UNLOCK_NOTIFY=1 -DSQLITE_ENABLE_DBSTAT_VTAB=1 \
	-DSQLITE_ENABLE_FTS3_PARENTHESIS=1 -DSQLITE_ENABLE_JSON1=1 \
	-DSQLITE_ENABLE_FTS4=1 -DSQLITE_ENABLE_STMTVTAB \
	-DSQLITE_ENABLE_MATH_FUNCTIONS -DSQLITE_MAX_VARIABLE_NUMBER=250000 \
	-DSQLITE_MAX_EXPR_DEPTH=10000"

%configure \
	--enable-fts3 \
	--enable-fts4 \
	--enable-fts5 \
	--enable-json1 \
	--enable-load-extension \
	--enable-rtree \
	--enable-threadsafe \
	--enable-threads-override-locks \
	--disable-tcl

sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

%make_build

%install
%make_install

%files
%doc README.txt
%defattr(-,root,root)
%{_bindir}/*
%{_includedir}/*
%{_libdir}/*
%{_mandir}/*

%files documentation
%doc ../%{name}-doc-%{srcver}/*

%changelog
* Thu Jul 01 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 3.36.0-1
- Initial package
