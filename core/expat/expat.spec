Name:		expat
Version:	2.4.1
Release:	1
Summary:	An XML parser library
License:	MIT
URL:		https://libexpat.github.io/

Source0:	https://github.com/libexpat/libexpat/releases/download/R_%(echo %{version} | tr '.' '_')/%{name}-%{version}.tar.bz2

%description
cool package

%prep
%setup -q

%build
%configure \
	--without-docbook
%make_build

%install
%make_install

%files
%doc AUTHORS Changes changelog doc/reference.html doc/*.png doc/*.css examples/*.c
%license COPYING
%defattr(-,root,root)
%{_bindir}/*
%{_includedir}/*
%{_libdir}/*

%changelog
* Thu Jul 01 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 2.4.1-1
- Initial package
