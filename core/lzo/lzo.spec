%global _visibility_flags -fvisibility=default

Name:		lzo
Version:	2.10
Release:	1
Summary:	Data compression library with very fast (de)compression
License:	GPLv2+
URL:		http://www.oberhumer.com/opensource/lzo/

Source0:	http://www.oberhumer.com/opensource/lzo/download/%{name}-%{version}.tar.gz

%description
LZO is a portable lossless data compression library written in ANSI C.
It offers pretty fast compression and very fast decompression.
Decompression requires no memory. In addition there are slower
compression levels achieving a quite competitive compression ratio
while still decompressing at this very high speed.

%prep
%setup -q

%build
%configure \
	--enable-shared \
	--enable-static
%make_build

%{__cc} %{optflags} -fpic -Iinclude/lzo -o minilzo/minilzo.o -c minilzo/minilzo.c
%{__cc} %{build_ldflags} -shared -o libminilzo.so.0 -Wl,-soname,libminilzo.so.0 minilzo/minilzo.o

%install
%make_install

install -p -m 644 minilzo/minilzo.h "%{buildroot}"%{_includedir}/lzo
install -m 755 libminilzo.so.0 "%{buildroot}"%{_libdir}

ln -sf libminilzo.so.0 "%{buildroot}"%{_libdir}/libminilzo.so

%files
%doc AUTHORS THANKS NEWS minilzo/README.LZO doc/LZOAPI.TXT doc/LZO.FAQ doc/LZO.TXT
%license COPYING
%defattr(-,root,root)
%{_docdir}/*
%{_libdir}/*
%{_includedir}/*

%changelog
* Sat Jun 19 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 2.10-1
- Initial package
