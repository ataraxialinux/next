Name:		iana-etc
Version:	20210611
Release:	1
Summary:	/etc/protocols and /etc/services provided by IANA
License:	custom
URL:		https://www.iana.org/protocols/

Source0:	https://github.com/Mic92/iana-etc/releases/download/%{version}/%{name}-%{version}.tar.gz
Source1:	iana-etc.tmpfiles

BuildArch:	noarch

%description
/etc/protocols and /etc/services provided by IANA

%prep
%setup -q

%install
install -Dm644 services "%{buildroot}"%{_factorysysconfdir}/services
install -Dm644 protocols "%{buildroot}"%{_factorysysconfdir}/protocols
install -Dm644 "%{SOURCE1}" "%{buildroot}"%{_libdir}/tmpfiles.d/%{name}.conf

%files
%defattr(-,root,root)
%{_factorysysconfdir}/*
%{_libdir}/tmpfiles.d/*

%changelog
* Mon Jun 21 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 20210611-1
- Initial package
