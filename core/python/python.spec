%ifarch x86_64 aarch64 ppc64le ppc64
%define _with_lto --with-lto
%endif
%ifarch riscv64
%define _with_lto --without-lto
%endif

%define _lto_cflags %{nil}
%define _cfi_cflags %{nil}

%define pybasever %(echo %{version}| cut -c1,2,3)

Name:		python
Version:	3.9.5
Release:	1
Summary:	The Python programming language
License:	Python
URL:		https://www.python.org/

Source0:	https://www.python.org/ftp/python/%{version}/Python-%{version}.tar.xz

Patch0:		python-0004-Adjust-library-header-paths-for-cross-compilation.patch
Patch1:		python-0030-Fix-cross-compiling-the-uuid-module.patch
Patch2:		python-mpdecimal-2.5.1.patch
Patch3:		python-musl-find_library.patch
Patch4:		python-0001-Don-t-search-system-for-headers-libraries.patch

BuildRequires:	llvm
BuildRequires:	zlib-ng
BuildRequires:	bzip2
BuildRequires:	xz
BuildRequires:	libedit
BuildRequires:	gdbm
BuildRequires:	sqlite
BuildRequires:	openssl
BuildRequires:	libffi
BuildRequires:	expat
BuildRequires:	mpdecimal
BuildRequires:	util-linux

Provides:	/usr/bin/python
Provides:	/usr/bin/python3
Provides:	/usr/bin/python%{pybasever}

%description
Python is an accessible, high-level, dynamically typed, interpreted
programming language, designed with an emphasis on code readability.
It includes an extensive standard library, and has a vast ecosystem of
third-party libraries.

%prep
%setup -q -n Python-%{version}
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1

%if "%buildtype" == "cross"
%patch4 -p1
%endif

sed -i -e "s|^#.* /usr/local/bin/python|#!/usr/bin/python|" Lib/cgi.py
sed -i 's|/bin/bash|/bin/sh|' Lib/test/ziptestdata/header.sh
sed -i 's/-j0 //' Makefile.pre.in

rm -r \
	Modules/expat \
	Modules/_ctypes/darwin* \
	Modules/_ctypes/libffi* \
	Modules/_decimal/libmpdec

%build
export ac_cv_have_long_long_format=yes
export ac_cv_file__dev_ptmx=yes
export ac_cv_file__dev_ptc=yes
export ac_cv_working_tzset=yes
export ac_cv_prog_HAS_HG=/bin/true

%if "%buildtype" == "cross"
export ROOTFS="%{rootfs}"
%endif

%configure \
	%{_with_lto} \
	--with-dbmliborder=gdbm:ndbm \
	--with-ssl-default-suites=openssl \
	--with-system-expat \
	--with-system-ffi \
	--with-system-libmpdec \
	--with-tzpath=/usr/share/zoneinfo \
	--without-dtrace \
	--without-ensurepip \
	--without-valgrind \
	--enable-ipv6 \
	--enable-loadable-sqlite-extensions \
	--enable-shared
%make_build EXTRA_CFLAGS="%{optflags} -DTHREAD_STACK_SIZE=0x100000"

%install
%make_install EXTRA_CFLAGS="%{optflags} -DTHREAD_STACK_SIZE=0x100000"

ln -sf python3 "%{buildroot}"%{_bindir}/python
ln -sf python3-config "%{buildroot}"%{_bindir}/python-config
ln -sf idle3 "%{buildroot}"%{_bindir}/idle
ln -sf pydoc3 "%{buildroot}"%{_bindir}/pydoc

%if "%{buildtype}" == "cross"
for i in python%{pybasever} python3 python; do
	install -Dm755 "%{buildroot}"%{_bindir}/$i-config "%{tools}"/bin/$i-config
done

sed -i 's|prefix_real=$(installed_prefix "$0")|prefix_real="ROOTFS/usr"|' "%{tools}"/bin/python*-config
sed -i "s|ROOTFS|%{rootfs}|" "%{tools}"/bin/python*-config

sed -i "s/%{_xtarget}-//g" \
	"%{buildroot}"%{_libdir}/python%{pybasever}/_sysconfigdata_*_*.py \
	"%{buildroot}"%{_libdir}/python%{pybasever}/config-%{pybasever}*/Makefile
sed -i -e "s,%{rootfs},,g" \
	"%{buildroot}"%{_bindir}/python%{pybasever}-config
%endif

%files
%doc README.rst
%license LICENSE
%defattr(-,root,root)
%{_bindir}/*
%{_includedir}/*
%{_libdir}/*
%{_mandir}/*

%changelog
* Thu Jul 01 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 3.9.5-1
- Initial package
