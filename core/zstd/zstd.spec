Name:		zstd
Version:	1.5.0
Release:	1
Summary:	ZSTD compression library
License:	BSD and GPLv2
URL:		https://github.com/facebook/zstd/

Source0:	https://github.com/facebook/zstd/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:	zlib-ng
BuildRequires:	xz
BuildRequires:	lz4

%description
Zstd, short for Zstandard, is a fast lossless compression algorithm,
targeting real-time compression scenarios at zlib-level compression ratio.

%prep
%setup -q

%build
%set_build_flags
%make_build HAVE_PTHREAD=1 HAVE_ZLIB=1 HAVE_LZMA=1 HAVE_LZ4=1
%make_build -C contrib/pzstd

%install
%make_install PREFIX=%{_prefix}

install -m755 contrib/pzstd/pzstd "%{buildroot}"%{_bindir}/pzstd
install -m644 programs/zstd.1 "%{buildroot}"%{_mandir}/man1/pzstd.1

%files
%doc CHANGELOG README.md
%license COPYING LICENSE
%defattr(-,root,root)
%{_bindir}/*
%{_libdir}/*
%{_includedir}/*
%{_mandir}/*

%changelog
* Sat Jun 19 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 1.5.0-1
- Initial package
