Name:		acl
Version:	2.3.1
Release:	1
Summary:	Access control list utilities
License:	GPLv2+
URL:		https://savannah.nongnu.org/projects/acl/

Source0:	https://download-mirror.savannah.gnu.org/releases/%{name}/%{name}-%{version}.tar.gz

BuildRequires:	attr

%description
This package contains the getfacl and setfacl utilities needed for
manipulating access control lists.

%prep
%setup -q

%build
%configure \
	--disable-nls
%make_build

%install
%make_install

%files
%doc doc/CHANGES
%license doc/COPYING*
%defattr(-,root,root)
%{_bindir}/*
%{_docdir}/%{name}/*.txt
%{_docdir}/%{name}/COPYING*
%{_docdir}/%{name}/PORTING
%{_includedir}/*
%{_libdir}/*
%{_mandir}/*

%changelog
* Mon Jun 21 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 2.3.1-1
- Initial package
