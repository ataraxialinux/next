Name:		gettext-tiny
Version:	0.3.2
Release:	2
Summary:	A lightweight alternative to GNU Gettext toolchain
License:	custom
URL:		https://github.com/sabotage-linux/gettext-tiny/

Source0:	http://ftp.barfooze.de/pub/sabotage/tarballs/%{name}-%{version}.tar.xz

%description
A lightweight alternative to GNU Gettext toolchain. Provides stubs and
replacements for bloated GNU Gettext tools and libintl library.

%prep
%setup -q
sed -i 's/CFLAGS=-O0 -fPIC/CFLAGS  ?= -O0 -fPIC/' Makefile

%build
%set_build_flags
%make_build LIBINTL=MUSL prefix=%{_prefix}

%install
%make_install LIBINTL=MUSL prefix=%{_prefix}

%files
%doc README.md
%license LICENSE
%defattr(-,root,root)
%{_bindir}/*
%{_libdir}/*
%{_datadir}/*

%changelog
* Sat Jun 26 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 0.3.2-2
- Fix m4 directory
* Mon Jun 14 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 0.3.2-1
- Initial package
