%define _configure ../dist/configure
%global _visibility_flags -fvisibility=default

Name:		berkeley-db
Version:	5.3.28
Release:	1
Summary:	The Berkeley DB database library for C
License:	BSD and LGPLv2 and Sleepycat
URL:		https://www.oracle.com/database/berkeley-db/

Source0:	https://download.oracle.com/berkeley-db/db-%{version}.tar.gz

Patch0:		berkeley-db-0001-atomic-Rename-local-__atomic_compare_exchange-to-avo.patch
Patch1:		berkeley-db-0001-Fix-libc-compatibility-by-renaming-atomic_init-API.patch
Patch2:		berkeley-db-src_dbinc_db.patch

%description
The Berkeley Database (Berkeley DB) is a programmatic toolkit that
provides embedded database support for both traditional and
client/server applications. The Berkeley DB includes B+tree, Extended
Linear Hashing, Fixed and Variable-length record access methods,
transactions, locking, logging, shared memory caching, and database
recovery. The Berkeley DB supports C and C++ APIs. It is
used by many applications, including Python and Perl, so this should
be installed on all systems.

%package documentation
Summary: C development documentation files for the Berkeley DB library
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description documentation
The Berkeley Database (Berkeley DB) is a programmatic toolkit that
provides embedded database support for both traditional and
client/server applications. This package contains the header files,
libraries, and documentation for building programs which use the
Berkeley DB.

%prep
%setup -q -n db-%{version}
%patch0 -p1
%patch1 -p1
%patch2 -p0

%build
cd build_unix
%configure \
	--enable-compat185 \
	--enable-cxx \
	--enable-dbm
%make_build

%install
cd build_unix
%make_install

rm -rf "%{buildroot}"%{_prefix}/docs

%files
%doc README
%license LICENSE
%defattr(-,root,root)
%{_bindir}/*
%{_includedir}/*
%{_libdir}/*

%files documentation
%doc docs/*
%defattr(-,root,root)

%changelog
* Thu Jul 01 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 5.3.28-1
- Initial package
