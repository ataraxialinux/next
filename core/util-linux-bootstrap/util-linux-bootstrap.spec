%define upstream_version %{version}
%define upstream_major %(eval echo %{version} | %{__sed} -e 's/\([[:digit:]]*\)\.\([[:digit:]]*\)\.[[:digit:]]*$/\1.\2/')

Name:		util-linux-bootstrap
Version:	2.37
Release:	1
Summary:	util-linux bootstrap package
License:	GPLv2 and GPLv2+ and LGPLv2+ and BSD with advertising and Public Domain
URL:		https://en.wikipedia.org/wiki/Util-linux

Source0:	https://www.kernel.org/pub/linux/utils/util-linux/v%{upstream_major}/util-linux-%{upstream_version}.tar.xz

Provides:	libblkid
Provides:	libfdisk
Provides:	libmount
Provides:	libsmartcols
Provides:	libuuid
Provides:	util-linux

%description
util-linux bootstrap package

%prep
%setup -q -n util-linux-%{upstream_version}

%build
%configure \
	--without-audit \
	--without-python \
	--without-systemd \
	--without-systemdsystemunitdir \
	--enable-libblkid \
	--enable-libfdisk \
	--enable-libmount \
	--enable-libsmartcols \
	--enable-libuuid \
	--disable-all-programs
%make_build

%install
%make_install

%files
%defattr(-,root,root)
%{_includedir}/*
%{_libdir}/*

%changelog
* Thu Jul 01 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 2.37-1
- Initial package
