Name:		byacc
Version:	20210109
Release:	1
Summary:	Berkeley Yacc, a parser generator
License:	Public Domain
URL:		https://invisible-island.net/byacc/byacc.html

Source0:	https://invisible-mirror.net/archives/%{name}/%{name}-%{version}.tgz

%description
This package provides a parser generator utility that reads a grammar
specification from a file and generates an LR(1) parser for it.  The
parsers consist of a set of LALR(1) parsing tables and a driver
routine written in the C programming language.  It has a public domain
license which includes the generated C.

%prep
%setup -q

%build
%configure
%make_build

%install
%make_install

%files
%doc ACKNOWLEDGEMENTS CHANGES NEW_FEATURES NOTES NO_WARRANTY README
%license LICENSE
%defattr(-,root,root)
%{_bindir}/*
%{_mandir}/*

%changelog
* Mon Jun 21 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 20210109-1
- Initial package
