Name:		mpdecimal
Version:	2.5.1
Release:	1
Summary:	Library for general decimal arithmetic
License:	FBSDDL and BSD and GPL
URL:		https://www.bytereef.org/mpdecimal/index.html

Source0:	https://www.bytereef.org/software/mpdecimal/releases/%{name}-%{version}.tar.gz

%description
The package contains a library limpdec implementing General Decimal Arithmetic
Specification. The specification, written by Mike Cowlishaw from IBM, defines
a general purpose arbitrary precision data type together with rigorously
specified functions and rounding behavior.

%prep
%setup -q

%build
export LD="%{__cc}"

%configure
%make_build

%install
%make_install

rm -f "%{buildroot}"%{_docdir}/%{name}/LICENSE.txt

if [ "%{_pkgdocdir}" != "%{_docdir}/%{name}" ]; then
	install -d -m 0755 "%{buildroot}"%{_pkgdocdir}
	mv -v "%{buildroot}"%{_docdir}/%{name}/* "%{buildroot}"%{_pkgdocdir}/
fi

%files
%doc %{_pkgdocdir}
%license LICENSE.txt doc/LICENSE.txt
%defattr(-,root,root)
%{_includedir}/*
%{_libdir}/*

%changelog
* Thu Jul 01 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 2.5.1-1
- Initial package
