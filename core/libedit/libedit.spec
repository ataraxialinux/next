%global _visibility_flags -fvisibility=default

Name:		libedit
Version:	20210522+3.1
Release:	1
Summary:	The NetBSD Editline library
License:	MIT
URL:		https://www.thrysoee.dk/editline/

Source0:	https://www.thrysoee.dk/editline/%{name}-%(echo %{version} | tr '+' '-').tar.gz

BuildRequires:	netbsd-curses

%description
Libedit is an autotool- and libtoolized port of the NetBSD Editline library.
It provides generic line editing, history, and tokenization functions, similar
to those found in GNU Readline.

%prep
%setup -q -n %{name}-%(echo %{version} | tr '+' '-')

%build
export CFLAGS="-D__STDC_ISO_10646__=201103L %{optflags}"

%configure
%make_build

%install
%make_install

mkdir -p "%{buildroot}"%{_includedir}/readline
for i in chardefs.h history.h keymaps.h readline.h rlconf.h rlstdc.h rltypedefs.h tilde.h; do
	ln -sf ../editline/readline.h "%{buildroot}"%{_includedir}/readline/$i
done

for i in history readline; do
	ln -sf libedit.a "%{buildroot}"%{_libdir}/lib${i}.a
	ln -sf libedit.so "%{buildroot}"%{_libdir}/lib${i}.so
done

ln -sf libedit.pc "%{buildroot}"%{_libdir}/pkgconfig/readline.pc

%files
%doc ChangeLog THANKS examples/fileman.c examples/tc1.c examples/wtc1.c
%license COPYING
%defattr(-,root,root)
%{_libdir}/*
%{_includedir}/*
%{_mandir}/*

%changelog
* Mon Jun 14 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 1.2.2-1
- Initial package
