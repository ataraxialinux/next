%define _timezones africa antarctica asia australasia europe northamerica southamerica etcetera backward factory

Name:		tzdata
Version:	2021a
Release:	2
Summary:	Timezone data
License:	Public Domain
URL:		https://www.iana.org/time-zones/

Source0:	https://www.iana.org/time-zones/repository/releases/tzcode%{version}.tar.gz
Source1:	https://www.iana.org/time-zones/repository/releases/tzdata%{version}.tar.gz

%description
This package contains data files with rules for various timezones around
the world.

%prep
%setup -q -c -a 1
sed -i 's|sbin|bin|' Makefile

%build
%set_build_flags

%make_build TZDIR="/usr/share/zoneinfo"

cp zic zic-host
cp zdump zdump-host

%make_build clean
%make_build CC="${CC:-clang}" CFLAGS="%{optflags}" LDFLAGS="%{build_ldflags}" TZDIR="/usr/share/zoneinfo"

%install
%make_install ZIC='./zic-host'

for i in %{_timezones}; do
	./zic-host -b fat -d "%{buildroot}"%{_datadir}/zoneinfo $i
	./zic-host -b fat -d "%{buildroot}"%{_datadir}/zoneinfo/posix $i
	./zic-host -b fat -d "%{buildroot}"%{_datadir}/zoneinfo/right -L leapseconds $i
done

./zic-host -b fat -d "%{buildroot}"%{_datadir}/zoneinfo -p America/New_York

for i in iso3166.tab zone1970.tab zone.tab; do
	install -m444 $i "%{buildroot}"%{_datadir}/zoneinfo/$i
done

sed -i 's/bash/sh/' "%{buildroot}"%{_bindir}/tzselect

rm "%{buildroot}"%{_libdir}/libtz.a
rm "%{buildroot}"%{_sysconfdir}/localtime

%files
%doc README theory.html tz-link.html tz-art.html
%license LICENSE
%defattr(-,root,root)
%{_bindir}/*
%{_datadir}/*
%{_mandir}/*

%changelog
* Thu Jul 01 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 2021a-2
- Remove bash dependency
* Mon Jun 21 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 2021a-1
- Initial package
