Name:		xz
Version:	5.2.5
Release:	2
Summary:	LZMA compression utilities
License:	GPLv2+ and Public Domain
URL:		https://tukaani.org/xz/

Source0:	https://tukaani.org/%{name}/%{name}-%{version}.tar.xz

%description
LZMA is a general purpose compression algorithm designed by Igor Pavlov as
part of 7-Zip. It provides high compression ratio while keeping the
decompression speed fast. XZ is a implementation of the algorithm.

%prep
%setup -q

%build
%configure \
	--disable-nls
%make_build

%install
%make_install

sed -i 's|#!/bin/bash|#!/bin/sh|' \
	"%{buildroot}"/%{_bindir}/xzdiff \
	"%{buildroot}"/%{_bindir}/xzless \
	"%{buildroot}"/%{_bindir}/xzmore \
	"%{buildroot}"/%{_bindir}/xzgrep

%files
%doc README ChangeLog
%license COPYING*
%defattr(-,root,root)
%{_bindir}/*
%{_docdir}/*
%{_libdir}/*
%{_includedir}/*
%{_mandir}/*

%changelog
* Sat Jun 19 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 5.2.5-2
- Remove bash shebangs
* Fri Jun 18 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 5.2.5-1
- Initial package
