Name:		file
Version:	5.40
Release:	1
Summary:	Utility for determining file types
License:	BSD
URL:		https://www.darwinsys.com/file/

Source0:	https://astron.com/pub/%{name}/%{name}-%{version}.tar.gz

BuildRequires:	zlib-ng
BuildRequires:	bzip2
BuildRequires:	xz

%description
cool package

%prep
%setup -q

%build
%if "%{buildtype}" == "cross"
mkdir -p host
cd host
../configure \
	--prefix="%{_builddir}/%{name}-%{version}/host" \
	--disable-shared
make %{?_smp_mflags}
make install %{?_smp_mflags}

cd ..

export PATH="%{_builddir}/%{name}-%{version}/host/bin:$PATH"
%endif

%configure \
	--enable-fsect-man5
%make_build

%install
%make_install

%files
%doc ChangeLog README
%license COPYING
%defattr(-,root,root)
%{_bindir}/*
%{_datadir}/misc/magic.mgc
%{_includedir}/*
%{_libdir}/*
%{_mandir}/*

%changelog
* Mon Jun 21 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 5.40-1
- Initial package
