Name:		man-pages
Version:	2017.a
Release:	1
Summary:	POSIX manual pages
License:	custom
URL:		https://git.kernel.org/pub/scm/docs/man-pages/man-pages-posix.git/

Source0:	https://cdn.kernel.org/pub/linux/docs/man-pages/man-pages-posix/man-pages-posix-%(echo %{version} | tr '.' '-').tar.xz

BuildArch:	noarch

%description
This package contains large collection of manual pages defined by
POSIX standard.

%prep
%setup -q -n man-pages-posix-%(echo %{version} | sed 's/.a//')

%install
%make_install prefix=%{_prefix}

%files
%doc POSIX-COPYRIGHT README
%defattr(-,root,root)
%{_mandir}/*

%changelog
* Fri Jun 18 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 2017.a-1
- Initial package
