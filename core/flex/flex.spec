Name:		flex
Version:	2.6.4
Release:	3
Summary:	A tool for generating scanners (text pattern recognizers)
License:	custom
URL:		https://github.com/westes/flex

Source0:	https://github.com/westes/flex/releases/download/v%{version}/%{name}-%{version}.tar.gz
Patch0:		flex-rh1389575.patch

Requires:	m4

%description
The flex program generates scanners. Scanners are programs which can
recognize lexical patterns in text. Flex takes pairs of regular
expressions and C code as input and generates a C source file as
output. The output file is compiled and linked with a library to
produce an executable. The executable searches through its input for
occurrences of the regular expressions. When a match is found, it
executes the corresponding C code. Flex was designed to work with
both Yacc and Bison, and is used by many programs as part of their
build process.

%prep
%setup -q
%patch0 -p1
autoreconf -i

%build
export ac_cv_func_malloc_0_nonnull=yes
export ac_cv_func_realloc_0_nonnull=yes
export HELP2MAN=/bin/true

%configure \
	--disable-nls
%make_build

%install
%make_install

ln -sf flex "%{buildroot}"%{_bindir}/lex

%files
%doc README.md NEWS
%license COPYING
%defattr(-,root,root)
%{_bindir}/*
%{_docdir}/*
%{_includedir}/*
%{_infodir}/*
%{_libdir}/*
%{_mandir}/*

%changelog
* Wed Jun 30 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 2.6.4-3
- Remove `help2man` build dependency
* Wed Jun 30 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 2.6.4-2
- Add `help2man` build dependency
* Mon Jun 21 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 2.6.4-1
- Initial package
