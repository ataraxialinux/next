Name:		m4
Version:	1.4.19
Release:	1
Summary:	GNU macro processor
License:	GPLv3+
URL:		https://www.gnu.org/software/m4/

Source0:	https://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz

%description
A GNU implementation of the traditional UNIX macro processor.  M4 is
useful for writing text files which can be logically parsed, and is used
by many programs as part of their build process.  M4 has built-in
functions for including files, running shell commands, doing arithmetic,
etc.  The autoconf program needs m4 for generating configure scripts, but
not for running configure scripts.

%prep
%setup -q

%build
%configure \
	--disable-nls
%make_build

%install
%make_install

%files
%doc AUTHORS ChangeLog NEWS README THANKS TODO
%license COPYING
%defattr(-,root,root)
%{_bindir}/m4
%{_infodir}/*
%{_mandir}/*

%changelog
* Mon Jun 21 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 1.4.19-1
- Initial package
