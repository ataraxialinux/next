Name:		bison
Version:	3.7.6
Release:	1
Summary:	A GNU general-purpose parser generator
License:	GPLv3+
URL:		https://www.gnu.org/software/bison/

Source0:	https://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz

Requires:	m4

%description
Bison is a general purpose parser generator that converts a grammar
description for an LALR(1) context-free grammar into a C program to
parse that grammar. Bison can be used to develop a wide range of
language parsers, from ones used in simple desk calculators to complex
programming languages. Bison is upwardly compatible with Yacc, so any
correctly written Yacc grammar should work with Bison without any
changes. If you know Yacc, you shouldn't have any trouble using
Bison. You do need to be very proficient in C programming to be able
to use Bison. Bison is only needed on systems that are used for
development.

%prep
%setup -q

%build
M4=m4 \
%configure \
	--disable-nls
%make_build

%install
%make_install

rm \
	"%{buildroot}"%{_bindir}/yacc \
	"%{buildroot}"%{_mandir}/man1/yacc.1

%files
%doc AUTHORS ChangeLog NEWS README THANKS TODO
%license COPYING
%defattr(-,root,root)
%{_bindir}/*
%{_datadir}/aclocal/*
%{_datadir}/bison/*
%{_docdir}/*
%{_infodir}/*
%{_libdir}/*
%{_mandir}/*

%changelog
* Mon Jun 21 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 3.7.6-1
- Initial package
