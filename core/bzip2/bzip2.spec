Name:		bzip2
Version:	1.0.8
Release:	1
Summary:	File compression utility
License:	BSD
URL:		https://sourceware.org/bzip2/

Source0:	https://sourceware.org/pub/%{name}/%{name}-%{version}.tar.gz

Patch0:		bzip2-autoconf.patch

%description
Bzip2 is a freely available, patent-free, high quality data compressor.
Bzip2 compresses files to within 10 to 15 percent of the capabilities
of the best techniques available.  However, bzip2 has the added benefit
of being approximately two times faster at compression and six times
faster at decompression than those techniques.  Bzip2 is not the
fastest compression utility, but it does strike a balance between speed
and compression capability.

%prep
%setup -q
%patch0 -p0
autoreconf -vif

%build
%configure
%make_build

%install
%make_install

%files
%doc CHANGES README
%license LICENSE
%defattr(-,root,root)
%{_bindir}/*
%{_libdir}/*
%{_includedir}/*
%{_mandir}/*

%changelog
* Fri Jun 18 2021 Ishimoto Shinobu <nagakamira@gmail.com> - 1.0.8-1
- Initial package
