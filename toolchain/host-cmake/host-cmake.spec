Name:		cmake
Version:	3.20.3
Release:	1
Summary:	Host package for cmake
License:	BSD and MIT and zlib
URL:		https://cmake.org/

Source0:	https://cmake.org/files/v%(echo %{version} | cut -d'.' -f1-2)/%{name}-%{version}.tar.gz

%description

%prep
%setup -q
sed -i '/"lib64"/s/64//' Modules/GNUInstallDirs.cmake

%build
%set_build_flags

unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

./bootstrap \
	--prefix="%{tools}" \
	--mandir=/share/man \
	--docdir=/share/doc/cmake \
	--parallel=%(nproc)
%make_build
%make_build install

mkdir -p "%{tools}"/share/cmake
cat >> "%{tools}"/share/cmake/cmake.cross <<-EOF
	set(CMAKE_SYSROOT %{rootfs})
	set(CMAKE_C_COMPILER %{tools}/bin/%{_xtarget}-clang)
	set(CMAKE_CXX_COMPILER %{tools}/bin/%{_xtarget}-clang++)
	set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
	set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
	set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
	set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
EOF

%changelog
