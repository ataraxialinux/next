Name:		byacc
Version:	20210109
Release:	1
Summary:	Host package for byacc
License:	Public Domain
URL:		https://invisible-island.net/byacc/byacc.html

Source0:	https://invisible-mirror.net/archives/%{name}/%{name}-%{version}.tgz

%description

%prep
%setup -q

%build
%set_build_flags

unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

./configure \
	--prefix="%{tools}"
%make_build
%make_build install

%changelog
