Name:		kmod
Version:	29
Release:	1
Summary:	Host package for kmod
License:	GPLv2+
URL:		https://git.kernel.org/pub/scm/utils/kernel/kmod/kmod.git

Source0:	https://www.kernel.org/pub/linux/utils/kernel/kmod/%{name}-%{version}.tar.xz

%description

%prep
%setup -q

%build
%set_build_flags

unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

./configure \
	--prefix="%{tools}" \
	--with-openssl \
	--with-xz \
	--with-zlib \
	--with-zstd \
	--disable-manpages
%make_build
%make_build install

for tool in insmod lsmod rmmod depmod modprobe modinfo; do
	ln -sf kmod "%{tools}"/bin/$tool
done

%changelog
