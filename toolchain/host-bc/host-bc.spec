Name:		bc
Version:	4.0.2
Release:	1
Summary:	Host package for bc
License:	custom
URL:		https://github.com/gavinhoward/bc

Source0:	https://github.com/gavinhoward/bc/releases/download/%{version}/%{name}-%{version}.tar.xz

%description

%prep
%setup -q

%build
%set_build_flags

unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

PREFIX='' \
./configure.sh \
	--disable-nls
%make_build PREFIX=''
%make_build PREFIX='' DESTDIR="%{tools}" install

%changelog
