Name:		gperf
Version:	3.1
Release:	1
Summary:	Host package for gperf
License:	GPLv3+
URL:		https://www.gnu.org/software/gperf/

Source0:	https://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.gz

%description

%prep
%setup -q

%build
%set_build_flags

unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

./configure \
	--prefix="%{tools}"
%make_build
%make_build install

%changelog
