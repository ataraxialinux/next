Name:		binutils
Version:	2.36.1
Release:	1
Summary:	Host package for binutils
License:	GPLv3+
URL:		https://www.gnu.org/software/binutils/

Source0:	https://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz

%description

%prep
%setup -q

%build
%set_build_flags

unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

case "%{_target_cpu}" in
	x86_64) archconfig="--enable-targets=x86_64-pep" ;;
esac

case "%{_target_cpu}" in
	*) hashconfig="--enable-default-hash-style=gnu"  ;;
esac

mkdir -p build
cd build

../configure \
	--prefix="%{tools}" \
	--target=%{_xtarget} $archconfig $hashconfig \
	--with-bugurl="https://gitlab.com/ataraxialinux/ataraxia/-/issues" \
	--with-sysroot="%{rootfs}" \
	--with-lib-path="%{rootfs}/usr/lib" \
	--with-pic \
	--with-system-zlib \
	--enable-64-bit-bfd \
	--enable-deterministic-archives \
	--enable-lto \
	--enable-plugins \
	--enable-relro \
	--enable-threads \
	--disable-nls \
	--disable-multilib \
	--disable-werror

%make_build configure-host
%make_build all-gas all-binutils
%make_build install-gas

install -m755 binutils/elfedit "%{tools}"/bin/%{_xtarget}-elfedit

%changelog
