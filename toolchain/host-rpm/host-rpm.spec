Name:		rpm
Version:	4.16.1.3
Release:	1
Summary:	Host package for rpm
License:	GPLv2+
URL:		http://www.rpm.org/

Source0:	http://ftp.rpm.org/releases/rpm-%(echo %{version} | cut -d'.' -f1-2).x/rpm-%{version}.tar.bz2
Source1:	brp-cleanup
Source2:	brp-libtool
Source3:	brp-strip
Source4:	find-provides
Source5:	find-requires
Source6:	macros
Source7:	rpmrc

Patch0:		rpm-0001-Fix-build-with-musl-C-library.patch
Patch1:		rpm-0004-skip-pkgconfig-dep.patch

%description

%prep
%setup -q
%patch0 -p1
%patch1 -p1

%build
unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

autoreconf -fi
./configure \
	--prefix="%{tools}" \
	--with-acl \
	--with-cap \
	--with-lua \
	--with-crypto=openssl \
	--with-vendor=ataraxia \
	--enable-sqlite \
	--enable-zstd \
	--disable-bdb \
	--disable-nls \
	--disable-plugins \
	--disable-shared \
	PYTHON=python3
%make_build
%make_build install

install -Dm755 "%{SOURCE1}" "%{tools}"/lib/rpm/ataraxia/brp-cleanup
install -Dm755 "%{SOURCE2}" "%{tools}"/lib/rpm/ataraxia/brp-libtool
install -Dm755 "%{SOURCE3}" "%{tools}"/lib/rpm/ataraxia/brp-strip
install -Dm755 "%{SOURCE4}" "%{tools}"/lib/rpm/ataraxia/find-provides
install -Dm755 "%{SOURCE5}" "%{tools}"/lib/rpm/ataraxia/find-requires

install -Dm644 "%{SOURCE6}" "%{tools}"/lib/rpm/ataraxia/macros
install -Dm644 "%{SOURCE7}" "%{tools}"/lib/rpm/ataraxia/rpmrc

sed -i "s|PPREFIX|%{tools}|" \
	"%{tools}"/lib/rpm/ataraxia/macros \
	"%{tools}"/lib/rpm/ataraxia/find-provides \
	"%{tools}"/lib/rpm/ataraxia/find-requires

%changelog
