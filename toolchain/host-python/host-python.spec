Name:		python
Version:	3.9.5
Release:	1
Summary:	Host package for python
License:	Python
URL:		https://www.python.org/

Source0:	https://www.python.org/ftp/python/%{version}/Python-%{version}.tar.xz

Patch0:		python-0001-distutils-sysconfig-append-STAGING_LIBDIR-python-sys.patch
Patch1:		python-12-distutils-prefix-is-inside-staging-area.patch

%description

%prep
%setup -q -n Python-%{version}
%patch0 -p1
%patch1 -p1

%build
%set_build_flags

unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

./configure \
	--prefix="%{tools}"
%make_build
%make_build install

ln -sf python3 "%{tools}"/bin/python
ln -sf python3-config "%{tools}"/bin/python-config
ln -sf idle3 "%{tools}"/bin/idle
ln -sf pydoc3 "%{tools}"/bin/pydoc

%changelog
