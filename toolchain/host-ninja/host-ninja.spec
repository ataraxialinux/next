Name:		ninja
Version:	1.10.2
Release:	1
Summary:	Host package for ninja
License:	ASL 2.0
URL:		https://ninja-build.org/

Source0:	https://github.com/ninja-build/ninja/archive/v%{version}/%{name}-%{version}.tar.gz

%description

%prep
%setup -q

%build
%set_build_flags

unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

python3 configure.py --bootstrap

install -m755 ninja "%{tools}"/bin/ninja

%changelog
