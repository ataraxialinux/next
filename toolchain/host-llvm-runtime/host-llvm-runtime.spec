Name:		llvm
Version:	12.0.0
Release:	1
Summary:	Host package for llvm
License:	ASL 2.0
URL:		https://llvm.org/

Source0:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{version}/llvm-%{version}.src.tar.xz
Source1:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{version}/clang-%{version}.src.tar.xz
Source2:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{version}/clang-tools-extra-%{version}.src.tar.xz
Source3:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{version}/lld-%{version}.src.tar.xz
Source4:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{version}/compiler-rt-%{version}.src.tar.xz
Source5:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{version}/libunwind-%{version}.src.tar.xz
Source6:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{version}/libcxx-%{version}.src.tar.xz
Source7:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{version}/libcxxabi-%{version}.src.tar.xz

Patch0:		clang-0001-add-support-for-Ataraxia-GNU-Linux.patch
Patch1:		clang-0002-PowerPC-use-ELFv2-ABI.patch
Patch2:		clang-0003-RISCV-disable-relaxations.patch
Patch3:		clang-0004-Link-against-libexecinfo.patch
Patch4:		clang-0005-move-dynamic-linker-in-usr-lib.patch

Patch5:		clang-tools-extra-0001-PATCH-clang-tools-extra-Make-clangd-CompletionModel-.patch

Patch6:		compiler-rt-0001-fixes-for-musl-libc.patch
Patch7:		compiler-rt-0002-build-crt-on-PowerPC.patch

Patch8:		libcxx-0001-fix-musl-locale.patch
Patch9:		libcxx-0002-force-link-against-compiler-rt-builtins.patch

Patch10:	libcxxabi-0001-don-t-link-against-__cxa_thread_atexit_impl.patch
Patch11:	libcxxabi-0002-force-link-against-compiler-rt-builtins.patch

Patch12:	lld-0001-ELF-use-SHA1-by-default.patch
Patch13:	lld-0002-PATCH-lld-Import-compact_unwind_encoding.h-from-libu.patch

Patch14:	llvm-0001-PowerPC-use-ELFv2-ABI.patch

%description

%prep
%setup -q -T -b 1 -n clang-%{version}.src
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1

%setup -q -T -b 2 -n clang-tools-extra-%{version}.src
%patch5 -p1

%setup -q -T -b 2 -n clang-tools-extra-%{version}.src
%patch5 -p1

%setup -q -T -b 4 -n compiler-rt-%{version}.src
%patch6 -p1
%patch7 -p1

%setup -q -T -b 6 -n libcxx-%{version}.src
%patch8 -p1
%patch9 -p1

%setup -q -T -b 7 -n libcxxabi-%{version}.src
%patch10 -p1
%patch11 -p1

%setup -q -T -b 5 -n libunwind-%{version}.src

%setup -q -T -b 3 -n lld-%{version}.src
%patch12 -p1
%patch13 -p1

%setup -q -T -b 0 -n llvm-%{version}.src
%patch14 -p1

cp -a ../clang-%{version}.src tools/clang
cp -a ../clang-tools-extra-%{version}.src tools/clang/tools/extra
cp -a ../lld-%{version}.src tools/lld
cp -a ../compiler-rt-%{version}.src projects/compiler-rt
cp -a ../libunwind-%{version}.src projects/libunwind
cp -a ../libcxx-%{version}.src projects/libcxx
cp -a ../libcxxabi-%{version}.src projects/libcxxabi

for i in libunwind libcxx libcxxabi; do
	[ -L "../$i" ] && rm ../$i
	ln -sf $i-%{version}.src ../$i
done

%build
%set_build_flags

SAFE_FLAGS="%{optflags}"
SAFE_FLAGS="$(echo $SAFE_FLAGS | sed 's/-flto//')"
SAFE_FLAGS="$(echo $SAFE_FLAGS | sed 's/-fvisibility=hidden//')"
SAFE_FLAGS="$(echo $SAFE_FLAGS | sed 's/-fsanitize=cfi//')"
SAFE_FLAGS="$(echo $SAFE_FLAGS | sed 's/-fsanitize=safe-stack//')"
SAFE_FLAGS="$(echo $SAFE_FLAGS | sed 's/-fsanitize=shadow-call-stack//')"
SAFE_FLAGS="$(echo $SAFE_FLAGS | sed 's/-fsanitize=scudo//')"

case "%{_target_cpu}" in
	x86_64)
		LARCH="x86_64"
		LTARGET="X86"
		;;
	aarch64)
		LARCH="aarch64"
		LTARGET="AArch64"
		;;
	ppc64le)
		LARCH="powerpc64le"
		LTARGET="PowerPC"
		;;
	ppc64)
		LARCH="powerpc64"
		LTARGET="PowerPC"
		;;
	riscv64)
		LARCH="riscv64"
		LTARGET="RISCV"
		;;
	*)
		echo 'Architecture called `'%{_target_cpu}'` is not supported by Ataraxia GNU/Linux'
		exit 1
		;;
esac

echo "Building 1 stage libunwind"
cd "%{_builddir}"
rm -rf build-libunwind
mkdir -p build-libunwind
cd build-libunwind

CFLAGS="$SAFE_FLAGS" \
CXXFLAGS="$SAFE_FLAGS" \
cmake "%{_builddir}/libunwind-%{version}.src" \
	-DCMAKE_CROSSCOMPILING=ON \
	-DCMAKE_INSTALL_PREFIX=/usr \
	-DCMAKE_BUILD_TYPE=MinSizeRel \
	-DCMAKE_C_COMPILER_TARGET="%{_xtarget}" \
	-DCMAKE_ASM_COMPILER_TARGET="%{_xtarget}" \
	-DCMAKE_C_COMPILER="%{tools}/bin/%{_xtarget}-clang" \
	-DCMAKE_CXX_COMPILER="%{tools}/bin/%{_xtarget}-clang++" \
	-DCMAKE_AR="%{tools}/bin/%{_xtarget}-ar" \
	-DCMAKE_NM="%{tools}/bin/%{_xtarget}-nm" \
	-DCMAKE_TRY_COMPILE_TARGET_TYPE=STATIC_LIBRARY \
	-DLLVM_PATH="%{_builddir}/llvm-%{version}.src" \
	-DLIBUNWIND_USE_COMPILER_RT=ON \
	-Wno-dev -G Ninja
%ninja_build
DESTDIR="%{rootfs}" ninja install

echo "Building 1 stage libc++abi"
cd "%{_builddir}"
rm -rf build-libcxxabi
mkdir -p build-libcxxabi
cd build-libcxxabi

CFLAGS="$SAFE_FLAGS" \
CXXFLAGS="$SAFE_FLAGS" \
cmake "%{_builddir}/libcxxabi-%{version}.src" \
	-DCMAKE_CROSSCOMPILING=ON \
	-DCMAKE_INSTALL_PREFIX=/usr \
	-DCMAKE_BUILD_TYPE=MinSizeRel \
	-DCMAKE_C_COMPILER_TARGET="%{_xtarget}" \
	-DCMAKE_C_COMPILER="%{tools}/bin/%{_xtarget}-clang" \
	-DCMAKE_CXX_COMPILER="%{tools}/bin/%{_xtarget}-clang++" \
	-DCMAKE_AR="%{tools}/bin/%{_xtarget}-ar" \
	-DCMAKE_NM="%{tools}/bin/%{_xtarget}-nm" \
	-DCMAKE_TRY_COMPILE_TARGET_TYPE=STATIC_LIBRARY \
	-DLLVM_PATH="%{_builddir}/llvm-%{version}.src" \
	-DLIBCXXABI_USE_LLVM_UNWINDER=ON \
	-DLIBCXXABI_USE_COMPILER_RT=ON \
	-Wno-dev -G Ninja
%ninja_build
DESTDIR="%{rootfs}" ninja install

echo "Building 1 stage libc++"
cd "%{_builddir}"
rm -rf build-libcxx
mkdir -p build-libcxx
cd build-libcxx

CFLAGS="$SAFE_FLAGS" \
CXXFLAGS="$SAFE_FLAGS" \
cmake "%{_builddir}/libcxx-%{version}.src" \
	-DCMAKE_CROSSCOMPILING=ON \
	-DCMAKE_INSTALL_PREFIX=/usr \
	-DCMAKE_BUILD_TYPE=MinSizeRel \
	-DCMAKE_C_COMPILER_TARGET="%{_xtarget}" \
	-DCMAKE_C_COMPILER="%{tools}/bin/%{_xtarget}-clang" \
	-DCMAKE_CXX_COMPILER="%{tools}/bin/%{_xtarget}-clang++" \
	-DCMAKE_AR="%{tools}/bin/%{_xtarget}-ar" \
	-DCMAKE_NM="%{tools}/bin/%{_xtarget}-nm" \
	-DCMAKE_TRY_COMPILE_TARGET_TYPE=STATIC_LIBRARY \
	-DLLVM_PATH="%{_builddir}/llvm-%{version}.src" \
	-DLIBCXX_CXX_ABI=libcxxabi \
	-DLIBCXX_CXX_ABI_INCLUDE_PATHS="%{_builddir}/libcxxabi-%{version}.src/include" \
	-DLIBCXX_HAS_MUSL_LIBC=ON \
	-DLIBCXX_USE_COMPILER_RT=ON \
	-Wno-dev -G Ninja

sed -i 's/-latomic//g' build.ninja

%ninja_build
DESTDIR="%{rootfs}" ninja install

echo "Building LLVM sanitizers"
cd "%{_builddir}"
rm -rf compiler-rt-sanitizers
mkdir -p compiler-rt-sanitizers
cd compiler-rt-sanitizers

CFLAGS="$SAFE_FLAGS" \
CXXFLAGS="$SAFE_FLAGS" \
cmake "%{_builddir}"/compiler-rt-%{version}.src \
	-DCMAKE_C_COMPILER_TARGET="%{_xtarget}" \
	-DCMAKE_ASM_COMPILER_TARGET="%{_xtarget}" \
	-DCMAKE_C_COMPILER="%{tools}/bin/%{_xtarget}-clang" \
	-DCMAKE_CXX_COMPILER="%{tools}/bin/%{_xtarget}-clang++" \
	-DCMAKE_AR="%{tools}/bin/%{_xtarget}-ar" \
	-DCMAKE_NM="%{tools}/bin/%{_xtarget}-nm" \
	-DCMAKE_RANLIB="%{tools}/bin/%{_xtarget}-ranlib" \
	-DCMAKE_TRY_COMPILE_TARGET_TYPE=STATIC_LIBRARY \
	-DLLVM_CONFIG_PATH="%{tools}/bin/llvm-config" \
	-DCOMPILER_RT_DEFAULT_TARGET_ONLY=ON \
	-DCOMPILER_RT_BUILD_LIBFUZZER=OFF \
	-DCOMPILER_RT_HAS_VERSION_SCRIPT=OFF \
	-DCOMPILER_RT_STANDALONE_BUILD=ON \
	-DCOMPILER_RT_USE_BUILTINS_LIBRARY=ON \
	-DCOMPILER_RT_USE_LIBCXX=ON \
	-DSANITIZER_CXX_ABI="libc++" \
	-Wno-dev -G Ninja
%ninja_build

for i in lib/linux/*; do
	install -Dm644 "$i" "%{tools}"/lib/clang/%{version}/lib/linux/$(basename $i)
done
for i in share/*; do
	install -Dm644 "$i" "%{tools}"/lib/clang/%{version}/share/$(basename $i)
done

echo "Building optimized LLVM runtime"
cd "%{_builddir}"
rm -rf build-final
mkdir -p build-final
cd build-final

cmake "%{_builddir}/llvm-%{version}.src" \
	-DCMAKE_CROSSCOMPILING=ON \
	-DCMAKE_INSTALL_PREFIX=/usr \
	-DCMAKE_BUILD_TYPE=MinSizeRel \
	-DCMAKE_C_COMPILER_TARGET="%{_xtarget}" \
	-DCMAKE_ASM_COMPILER_TARGET="%{_xtarget}" \
	-DCMAKE_C_COMPILER="%{tools}/bin/%{_xtarget}-clang" \
	-DCMAKE_CXX_COMPILER="%{tools}/bin/%{_xtarget}-clang++" \
	-DCMAKE_AR="%{tools}/bin/%{_xtarget}-ar" \
	-DCMAKE_NM="%{tools}/bin/%{_xtarget}-nm" \
	-DCMAKE_TRY_COMPILE_TARGET_TYPE=STATIC_LIBRARY \
	-DLLVM_CONFIG_PATH="%{tools}/bin/llvm-config" \
	-DLIBCXX_CXX_ABI=libcxxabi \
	-DLIBCXX_HAS_MUSL_LIBC=ON \
	-DLIBCXX_USE_COMPILER_RT=ON \
	-DLIBCXXABI_USE_LLVM_UNWINDER=ON \
	-DLIBCXXABI_USE_COMPILER_RT=ON \
	-DLIBUNWIND_USE_COMPILER_RT=ON \
	-DLLVM_APPEND_VC_REV=OFF \
	-DLLVM_DEFAULT_TARGET_TRIPLE=%{_xtarget} \
	-DLLVM_ENABLE_PROJECTS="libunwind;libcxx;libcxxabi" \
	-DLLVM_TARGETS_TO_BUILD=$LTARGET \
	-Wno-dev -G Ninja

sed -i 's/-latomic//g' build.ninja

%ninja_build unwind cxxabi cxx
DESTDIR="%{rootfs}" ninja install-unwind install-cxxabi install-cxx

%changelog
