Name:		musl
Version:	1.2.2
Release:	1
Summary:	Host package for musl headers
License:	MIT
URL:		https://musl.libc.org/

Source0:	https://musl.libc.org/releases/%{name}-%{version}.tar.gz

%description

%prep
%setup -q

%build
case "%{_target_cpu}" in
	x86_64)
		MARCH="x86_64"
		;;
	aarch64)
		MARCH="aarch64"
		;;
	ppc64le)
		MARCH="powerpc64"
		;;
	ppc64)
		MARCH="powerpc64"
		;;
	riscv64)
		MARCH="riscv64"
		;;
	*)
		echo 'Architecture called `'%{_target_cpu}'` is not supported by Ataraxia GNU/Linux'
		exit 1
		;;
esac

%make_build ARCH=$MARCH prefix=%{_prefix} DESTDIR="%{rootfs}" install-headers

%changelog
