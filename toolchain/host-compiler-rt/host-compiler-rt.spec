%define _lto_cflags %{nil}
%define _cfi_cflags %{nil}
%define _safestack_cflags %{nil}
%define _scs_cflags %{nil}
%define _scudo_cflags %{nil}

Name:		compiler-rt
Version:	12.0.0
Release:	1
Summary:	Host package for compiler-rt
License:	ASL 2.0
URL:		https://llvm.org/

Source0:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{version}/compiler-rt-%{version}.src.tar.xz

Patch0:		compiler-rt-0001-fixes-for-musl-libc.patch
Patch1:		compiler-rt-0002-build-crt-on-PowerPC.patch

%description

%prep
%setup -q -n compiler-rt-%{version}.src
%patch0 -p1
%patch1 -p1

%build
%set_build_flags

case "%{_target_cpu}" in
	x86_64) targets='crt builtins cfi' ;;
	aarch64) targets='crt builtins cfi' ;;
	ppc64le) targets='crt builtins' ;;
	ppc64) targets='crt builtins' ;;
	riscv64) targets='crt builtins' ;;
	*) echo 'Architecture called `'%{_target_cpu}'` is not supported by Ataraxia GNU/Linux'; exit 1 ;;
esac

mkdir -p compiler-rt-builtins-build
cd compiler-rt-builtins-build
cmake "%{_builddir}"/compiler-rt-%{version}.src \
	-DCMAKE_C_COMPILER_TARGET="%{_xtarget}" \
	-DCMAKE_ASM_COMPILER_TARGET="%{_xtarget}" \
	-DCMAKE_C_COMPILER="%{tools}/bin/%{_xtarget}-clang" \
	-DCMAKE_CXX_COMPILER="%{tools}/bin/%{_xtarget}-clang++" \
	-DCMAKE_AR="%{tools}/bin/%{_xtarget}-ar" \
	-DCMAKE_NM="%{tools}/bin/%{_xtarget}-nm" \
	-DCMAKE_RANLIB="%{tools}/bin/%{_xtarget}-ranlib" \
	-DCMAKE_TRY_COMPILE_TARGET_TYPE=STATIC_LIBRARY \
	-DLLVM_CONFIG_PATH="%{tools}/bin/llvm-config" \
	-DCOMPILER_RT_DEFAULT_TARGET_ONLY=ON \
	-DCOMPILER_RT_STANDALONE_BUILD=ON \
	-Wno-dev -G Ninja
%ninja_build $targets

for i in lib/linux/*; do
	install -Dm644 "$i" "%{tools}"/lib/clang/%{version}/lib/linux/$(basename $i)
done

case "%{_target_cpu}" in
	x86_64|aarch64)
		for i in share/*; do
			install -Dm644 "$i" "%{tools}"/lib/clang/%{version}/share/$(basename $i)
		done
		;;
esac

%changelog
