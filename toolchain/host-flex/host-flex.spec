Name:		flex
Version:	2.6.4
Release:	1
Summary:	Host package for flex
License:	custom
URL:		https://github.com/westes/flex
	
Source0:	https://github.com/westes/flex/releases/download/v%{version}/%{name}-%{version}.tar.gz
Patch0:		flex-rh1389575.patch

%description

%prep
%setup -q
%patch0 -p1

%build
%set_build_flags

unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

export HELP2MAN=/bin/true

autoreconf -i
./configure \
	--prefix="%{tools}"
%make_build
%make_build install

ln -sf flex "%{tools}"/bin/lex

%changelog
