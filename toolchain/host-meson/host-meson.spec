Name:		meson
Version:	0.58.0
Release:	1
Summary:	Host package for meson
License:	ASL 2.0
URL:		https://mesonbuild.com/

Source0:	https://github.com/mesonbuild/meson/releases/download/%{version}/%{name}-%{version}.tar.gz

Patch0:		meson-8757.patch
Patch1:		meson-8761.patch

%description

%prep
%setup -q
%patch0 -p1
%patch1 -p1

%build
python3 setup.py build
python3 setup.py install --prefix="%{tools}" --optimize=1

case "%{_target_cpu}" in
	x86_64)       cpu="x86_64" ;;
	aarch64)      cpu="aarch64" ;;
	ppc64le)      cpu="ppc64" cpuextra="ppc64le" ;;
	ppc64)        cpu="ppc64" ;;
	riscv64)      cpu="riscv64" ;;
esac

[ -z "$cpuextra" ] && cpuextra="$cpu"

case "%{_target_cpu}" in
	x86_64|aarch64|ppc64le|riscv64) endian="little" ;;
	ppc64) endian="big" ;;
esac

mkdir -p "%{tools}"/share/meson
cat > "%{tools}"/share/meson/meson.cross <<-EOF
	[host_machine]
	system = 'linux'
	cpu_family = '$cpu'
	cpu = '$cpuextra'
	endian = '$endian'

	[binaries]
	c = '%{tools}/bin/%{_xtarget}-clang'
	cpp = '%{tools}/bin/%{_xtarget}-clang++'
	ar = '%{tools}/bin/%{_xtarget}-ar'
	ld = '%{tools}/bin/%{_xtarget}-ld'
	strip = '%{tools}/bin/%{_xtarget}-strip'
	objcopy = '%{tools}/bin/%{_xtarget}-objcopy'
	pkgconfig = '%{tools}/bin/%{_xtarget}-pkg-config'
EOF

%changelog
