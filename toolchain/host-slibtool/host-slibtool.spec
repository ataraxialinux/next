Name:		slibtool
Version:	0.5.34
Release:	1
Summary:	Host package for slibtool
License:	custom
URL:		https://dev.midipix.org/cross/slibtool/

Source0:	http://midipix.org/dl/slibtool/%{name}-%{version}.tar.xz
Source1:	compile
Source2:	config.guess
Source3:	config.sub
Source4:	depcomp
Source5:	install-sh
Source6:	libtoolize
Source7:	libtool.m4
Source8:	ltargz.m4
Source9:	ltdl.m4
Source10:	ltmain.sh
Source11:	lt~obsolete.m4
Source12:	ltoptions.m4
Source13:	ltsugar.m4
Source14:	ltversion.m4
Source15:	missing

%description

%prep
%setup -q

%build
%set_build_flags

unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

NATIVE_CC="%{tools}/bin/%{_xtarget}-clang" \
NATIVE_CXX="%{tools}/bin/%{_xtarget}-clang++" \
NATIVE_CPP="%{tools}/bin/%{_xtarget}-clang-cpp" \
NATIVE_HOST="%{_xtarget}" \
NATIVE_CFGHOST="%{_xtarget}" \
./configure \
	--prefix="%{tools}" \
	--sbindir="%{tools}"/bin
%make_build
%make_build install

ln -sf slibtool "%{tools}"/bin/libtool

mkdir -p "%{tools}"/share/libtoolize/AC_CONFIG_AUX_DIR \
	"%{tools}"/share/libtoolize/AC_CONFIG_MACRO_DIRS \
	"%{tools}"/share/aclocal/

for i in %{SOURCE14} %{SOURCE7} %{SOURCE8} %{SOURCE9} %{SOURCE12} %{SOURCE13} %{SOURCE11}; do
	install -Dm644 "$i" "%{tools}"/share/aclocal/$(basename $i)
	install -Dm644 "$i" "%{tools}"/share/libtoolize/AC_CONFIG_MACRO_DIRS/$(basename $i)
done
for i in %{SOURCE1} %{SOURCE4} %{SOURCE5} %{SOURCE15}; do
	install -Dm755 "$i" "%{tools}"/share/libtoolize/AC_CONFIG_AUX_DIR/$(basename $i)
done

install -Dm755 "%{SOURCE10}" "%{tools}"/share/libtoolize/AC_CONFIG_AUX_DIR/ltmain.sh

install -Dm755 "%{SOURCE3}" "%{tools}"/share/libtoolize/AC_CONFIG_AUX_DIR/config.sub
install -Dm755 "%{SOURCE2}" "%{tools}"/share/libtoolize/AC_CONFIG_AUX_DIR/config.guess

install -Dm755 "%{SOURCE6}" "%{tools}"/bin/libtoolize
sed -i "s,uncom_sysroot,%{tools},g" "%{tools}"/bin/libtoolize

%changelog
