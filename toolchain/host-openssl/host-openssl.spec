Name:		openssl
Version:	1.1.1k
Release:	1
Summary:	Host package for openssl
License:	OpenSSL and ASL 2.0	
URL:		https://www.openssl.org/

Source0:	https://www.openssl.org/source/%{name}-%{version}.tar.gz

%description

%prep
%setup -q

%build
%set_build_flags

unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

./config \
	--prefix="%{tools}" \
	--openssldir="%{tools}/etc/ssl" \
	--libdir=lib \
	no-shared no-zlib \
	no-async no-comp no-idea no-mdc2 no-rc5 no-ec2m \
	no-sm2 no-sm4 no-ssl2 no-ssl3 no-seed \
	no-weak-ssl-ciphers \
	-Wa,--noexecstack
%make_build
%make_build install

%changelog
