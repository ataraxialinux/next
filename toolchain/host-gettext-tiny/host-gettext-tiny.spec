Name:		gettext-tiny
Version:	0.3.2
Release:	1
Summary:	Host package for gettext-tiny
License:	custom
URL:		https://github.com/sabotage-linux/gettext-tiny/

Source0:	http://ftp.barfooze.de/pub/sabotage/tarballs/%{name}-%{version}.tar.xz

%description

%prep
%setup -q
sed -i 's,#!/bin/sh,#!/usr/bin/env sh,g' src/autopoint.in

%build
%set_build_flags

unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

%make_build prefix="%{tools}" install

%changelog
