Name:		pkgconf
Version:	1.6.3
Release:	1
Summary:	Host package for pkgconf
License:	ISC
URL:		https://git.sr.ht/~kaniini/pkgconf/

Source0:	https://distfiles.dereferenced.org/%{name}/%{name}-%{version}.tar.xz

Patch0:		pkgconf-0001-Only-prefix-with-the-sysroot-a-subset-of-variables.patch

%description

%prep
%setup -q
%patch0 -p1

%build
%set_build_flags

unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

LDFLAGS="-static" \
./configure \
	--prefix="%{tools}"
%make_build
%make_build install

cp pkgconf "%{tools}"/bin/pkg-config
cp pkgconf "%{tools}"/bin/%{_xtarget}-pkg-config
cp pkgconf "%{tools}"/bin/%{_xtarget}-pkgconf

%changelog
