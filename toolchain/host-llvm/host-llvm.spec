Name:		llvm
Version:	12.0.0
Release:	1
Summary:	Host package for llvm
License:	ASL 2.0
URL:		https://llvm.org/

Source0:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{version}/llvm-%{version}.src.tar.xz
Source1:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{version}/clang-%{version}.src.tar.xz
Source2:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{version}/clang-tools-extra-%{version}.src.tar.xz
Source3:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{version}/lld-%{version}.src.tar.xz
Source4:	https://github.com/llvm/llvm-project/releases/download/llvmorg-%{version}/compiler-rt-%{version}.src.tar.xz

Patch0:		clang-0001-add-support-for-Ataraxia-GNU-Linux.patch
Patch1:		clang-0002-PowerPC-use-ELFv2-ABI.patch
Patch2:		clang-0003-RISCV-disable-relaxations.patch
Patch3:		clang-0004-Link-against-libexecinfo.patch
Patch4:		clang-0005-move-dynamic-linker-in-usr-lib.patch

Patch5:		clang-tools-extra-0001-PATCH-clang-tools-extra-Make-clangd-CompletionModel-.patch

Patch6:		compiler-rt-0001-fixes-for-musl-libc.patch
Patch7:		compiler-rt-0002-build-crt-on-PowerPC.patch

Patch8:		lld-0001-ELF-use-SHA1-by-default.patch
Patch9:		lld-0002-PATCH-lld-Import-compact_unwind_encoding.h-from-libu.patch

Patch10:	llvm-0001-PowerPC-use-ELFv2-ABI.patch

%description

%prep
%setup -q -T -b 1 -n clang-%{version}.src
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1

%setup -q -T -b 2 -n clang-tools-extra-%{version}.src
%patch5 -p1

%setup -q -T -b 4 -n compiler-rt-%{version}.src
%patch6 -p1
%patch7 -p1

%setup -q -T -b 3 -n lld-%{version}.src
%patch8 -p1
%patch9 -p1

%setup -q -T -b 0 -n llvm-%{version}.src
%patch10 -p1

cp -a ../clang-%{version}.src tools/clang
cp -a ../clang-tools-extra-%{version}.src tools/clang/tools/extra
cp -a ../lld-%{version}.src tools/lld
cp -a ../compiler-rt-%{version}.src projects/compiler-rt

%build
%set_build_flags

unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

case "%{_target_cpu}" in
	x86_64)
		LARCH="x86_64"
		LTARGET="X86"
		;;
	aarch64)
		LARCH="aarch64"
		LTARGET="AArch64"
		;;
	ppc64le)
		LARCH="powerpc64le"
		LTARGET="PowerPC"
		;;
	ppc64)
		LARCH="powerpc64"
		LTARGET="PowerPC"
		;;
	riscv64)
		LARCH="riscv64"
		LTARGET="RISCV"
		;;
	*)
		echo 'Architecture called `'%{_target_cpu}'` is not supported by Ataraxia GNU/Linux'
		exit 1
		;;
esac

mkdir -p build
cd build

cmake "%{_builddir}"/llvm-%{version}.src \
	-DCMAKE_INSTALL_PREFIX="%{tools}" \
	-DCMAKE_BUILD_TYPE=MinSizeRel \
	-DCLANG_BUILD_EXAMPLES=OFF \
	-DCLANG_DEFAULT_CXX_STDLIB=libc++ \
	-DCLANG_DEFAULT_LINKER=lld \
	-DCLANG_DEFAULT_OPENMP_RUNTIME=libomp \
	-DCLANG_DEFAULT_RTLIB=compiler-rt \
	-DCLANG_DEFAULT_UNWINDLIB=libunwind \
	-DCLANG_INCLUDE_DOCS=OFF \
	-DCLANG_INCLUDE_TESTS=OFF \
	-DCLANG_PLUGIN_SUPPORT=ON \
	-DCLANG_VENDOR=Ataraxia \
	-DCOMPILER_RT_BUILD_BUILTINS=OFF \
	-DCOMPILER_RT_BUILD_CRT=OFF \
	-DCOMPILER_RT_BUILD_LIBFUZZER=OFF \
	-DCOMPILER_RT_BUILD_MEMPROF=OFF \
	-DCOMPILER_RT_BUILD_PROFILE=OFF \
	-DCOMPILER_RT_BUILD_SANITIZERS=OFF \
	-DCOMPILER_RT_BUILD_XRAY=OFF \
	-DLLVM_EXTERNAL_CLANG_TOOLS_EXTRA_SOURCE_DIR="%{_builddir}/clang-tools-extra-%{version}.src" \
	-DENABLE_LINKER_BUILD_ID=ON \
	-DLLVM_APPEND_VC_REV=OFF \
	-DLLVM_BUILD_EXAMPLES=OFF \
	-DLLVM_BUILD_DOCS=OFF \
	-DLLVM_BUILD_TESTS=OFF \
	-DLLVM_ENABLE_DOXYGEN=OFF \
	-DLLVM_ENABLE_SPHINX=OFF \
	-DLLVM_DEFAULT_TARGET_TRIPLE="%{_xtarget}" \
	-DLLVM_TARGET_ARCH="$LARCH" \
	-DLLVM_TARGETS_TO_BUILD="$LTARGET" \
	-DDEFAULT_SYSROOT="%{rootfs}" \
	-DOCAMLFIND=NO \
	-DGO_EXECUTABLE=GO_EXECUTABLE-NOTFOUND \
	-Wno-dev -G Ninja

%ninja_build
%ninja_build install

clang="$(readlink %{tools}/bin/clang)"

cd "%{tools}/bin"
for i in cc c++ clang clang++ clang-cpp cpp; do
	cp $clang %{_xtarget}-$i
done

for i in ar dwp nm objcopy objdump size strings; do
	cp llvm-$i %{_xtarget}-$i
done

cp lld %{_xtarget}-ld
cp lld %{_xtarget}-ld.lld
cp llvm-symbolizer %{_xtarget}-addr2line
cp llvm-cxxfilt %{_xtarget}-c++filt
cp llvm-cov %{_xtarget}-gcov
cp llvm-ar %{_xtarget}-ranlib
cp llvm-readobj %{_xtarget}-readelf
cp llvm-objcopy %{_xtarget}-strip

rm -f $clang clang clang++ clang-cl clang-cpp lld-link ld.lld ld64.lld ld64.lld.darwinnew wasm-ld lld

%changelog
