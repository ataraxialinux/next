Name:		automake
Version:	1.16.3
Release:	1
Summary:	Host package for automake
License:	GPLv2+ and GFDL and Public Domain and MIT
URL:		https://www.gnu.org/software/automake/

Source0:	https://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz

%description

%prep
%setup -q

%build
%set_build_flags

unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

./configure \
	--prefix="%{tools}"
%make_build
%make_build install

%changelog
