Name:		texinfo
Version:	6.7
Release:	1
Summary:	Host package for texinfo
License:	GPLv3+
URL:		https://www.gnu.org/software/texinfo/

Source0:	https://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz

%description

%prep
%setup -q

%build
%set_build_flags

unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

./configure \
	--prefix="%{tools}"
%make_build
%make_build install

%changelog
