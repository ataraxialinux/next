Name:		autoconf
Version:	2.71
Release:	1
Summary:	Host package for autoconf
License:	GPLv2+ and GPLv3+ and GFDL
URL:		https://www.gnu.org/software/autoconf/

Source0:	https://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz

%description

%prep
%setup -q

%build
%set_build_flags

unset CFLAGS CXXFLAGS FFLAGS FCFLAGS RUSTFLAGS LDFLAGS

./configure \
	--prefix="%{tools}"
%make_build
%make_build install

%changelog
